#include"Heap.h"
int main()
{
	Heap hp;
	int a[] = { 15,19,28,34,65,18,49,25,37,27 };
	int len = sizeof(a) / sizeof(a[0]);
	HeapInit(&hp,a,len);
	HeapPrint(&hp);
	HeapPush(&hp, 14);
	HeapPrint(&hp);
	HeapPop(&hp);
	HeapPrint(&hp);
	printf("%d\n", HeapTop(&hp));
	printf("%d\n", HeapSize(&hp));
	HeapDestroy(&hp);
}
