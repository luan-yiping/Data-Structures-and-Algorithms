#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include<stdbool.h>
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* _a;
	int _size;    // 有效数据
	int _capacity; // 堆的容量
}Heap;
// 交换
void Swap(HPDataType* child, HPDataType* parent);
// 向下调整
void AdjustDown(HPDataType* a, int n, int parent);
// 堆的初始化
void HeapInit(Heap* php, HPDataType* a,int n);
// 堆的销毁
void HeapDestroy(Heap* php);
// 堆的插入
void HeapPush(Heap* php,HPDataType x);
// 向上调整
void AdjustUp(HPDataType* a, int child);
// 打印堆
void HeapPrint(Heap* php);
// 堆的删除
void HeapPop(Heap* php);
// 取堆顶的数据
HPDataType HeapTop(Heap* php);
// 堆的数据个数
int HeapSize(Heap* php);
// 堆的判空
bool HeapEmpty(Heap* php);