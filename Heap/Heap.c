#include"Heap.h"
void Swap(HPDataType* child, HPDataType* parent)
{
	int tmp = *child;
	*child = *parent;
	*parent = tmp;
}
// 打印堆
void HeapPrint(Heap* php)
{
	int i = 0;
	for (i = 0; i < php->_size; i++)
	{
		printf("%d ", php->_a[i]);
	}
	printf("\n");
}
// 向下调整
void AdjustDown(HPDataType* a, int n, int parent)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] < a[child])
			++child;
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			break;
		}
	}
}
// 堆的初始化
void HeapInit(Heap* php, HPDataType* a, int n)
{
	assert(php);
	php->_a = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (php->_a == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	memcpy(php->_a, a, sizeof(HPDataType) * n);
	php->_size = php->_capacity = n;
	// 建小堆
	int i = (n - 2) / 2;
	for (i = (n - 2) / 2; i >= 0; i--)
	{
		AdjustDown(php->_a, n, i);
	}
}
// 堆的销毁
void HeapDestroy(Heap* php)
{
	assert(php);
	free(php->_a);
	free(php);
	php = NULL;
}
// 向上调整
void AdjustUp(HPDataType* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}
// 堆的插入
void HeapPush(Heap* php, HPDataType x)
{
	assert(php);
	if (php->_size == php->_capacity)
	{
		php->_capacity *= 2;
		HPDataType* tmp = (HPDataType*)realloc(php->_a, sizeof(HPDataType) * php->_capacity);
		if (tmp == NULL)
		{
			printf("realloc failed\n");
			exit(-1);
		}
		php->_a = tmp;
	}
	php->_a[php->_size++] = x;
	AdjustUp(php->_a, php->_size - 1);
}
// 堆的删除
void HeapPop(Heap* php)
{
	assert(php);
	assert(php->_size > 0);
	Swap(&php->_a[0], &php->_a[php->_size - 1]);
	php->_size--;
	AdjustDown(php->_a, php->_size, 0);
}
// 取堆顶的数据
HPDataType HeapTop(Heap* php)
{
	assert(php);
	assert(php->_size > 0);
	return php->_a[0];
}
// 堆的数据个数
int HeapSize(Heap* php)
{
	assert(php);
	assert(php->_size > 0);
	return php->_size;
}
// 堆的判空
bool HeapEmpty(Heap* php)
{
	assert(php);
	return php->_size == 0;
    
}