typedef struct TreeNode TreeNode;

// 递归
void PrevOrder(TreeNode* root, int* ret, int* size)
{
    if (root == NULL)
        return NULL;
    ret[(*size)++] = root->val;
    PrevOrder(root->left, ret, size);
    PrevOrder(root->right, ret, size);
}
int* preorderTraversal(struct TreeNode* root, int* returnSize)
{
    int* ret = (int*)malloc(sizeof(int) * 100);
    int size = 0;
    PrevOrder(root, ret, &size);
    *returnSize = size;
    return ret;
}





// 迭代
// 使用一个栈来记录遍历过的结点
int* preorderTraversal(struct TreeNode* root, int* returnSize)
{
    if (root == NULL)
    {
        *returnSize = 0;
        return NULL;
    }
    // 记录遍历的结点
    TreeNode** Stack = (TreeNode**)malloc(sizeof(TreeNode*) * 100);
    // 记录结点数值
    int* ret = (int*)malloc(sizeof(int) * 100);
    int top = 0;
    TreeNode* cur = root;
    *returnSize = 0;
    while (top > 0 || cur != NULL)
    {
        while (cur != NULL)
        {
            ret[(*returnSize)++] = cur->val;
            Stack[top++] = cur;
            cur = cur->left;
        }
        cur = Stack[--top];
        cur = cur->right;
        // 若cur为空,则不断回退
    }
    free(Stack);
    return ret;
}