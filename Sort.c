#include<stdio.h>
#include<string.h>
#include"Stack.h"

void InsertSort(int* a, int n)
{
	int i = 0;
	for (i = 0; i < n - 1; i++)
	{
		// 有序区间[0,end]
		int end = i;
		// 将a[end + 1]插入到有序区间里
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (tmp < a[end])
			{
				// 将数据向后移动
				a[end + 1] = a[end];
				--end;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}
void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		// 最终gap为1进行直接插入排序
		gap = (gap / 3) + 1;
		int i = 0;
		// 对gap组数据进行希尔排序
		for (i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					--end;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

// 选择排序
void SelectSort(int* a, int n)
{
	int left = 0, right = n - 1;
	int i = left, minIndex = left, maxIndex = left;
	for (i = left; i <= right; i++)
	{
		if (a[i] < a[minIndex])
		{
			minIndex = i;
		}
		if (a[i] > a[maxIndex])
		{
			maxIndex = i;
		}

	}
}
void Swap(int* e1, int* e2)
{
	int tmp = *e1;
	*e1 = *e2;
	*e2 = tmp;
}
void PrintArray(int* a, int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
void BubbleSort(int* a, int n)
{
	int i = 0;
	// 控制内层循环的循环次数
	for (i = 0; i < n - 1; i++)
	{
		int j = 0, flag = 0;
		// 两两比较选择出最大的数冒到最后
		for (j = 0; j < n - 1 - i; j++)
		{
			if (a[j] > a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}
}
// 三数取中,选择a[left],a[right],a[mid]中既不是最大的，也不是最小的数
int MiddleIndex(int* a, int left, int right)
{
	int mid = (left + right) >> 1;
	if (a[left] < a[mid])
	{
		if (a[mid] < a[right])
			return mid;
		else if (a[left] < a[right])
			return right;
		else
			return left;
	}
	else
	{
		if (a[mid] > a[right])
			return mid;
		else if (a[left] < a[right])
			return left;
		else
			return right;
	}
}
// 单趟排序
int PartSort1(int* a, int left, int right)
{
	// 在有序的情况下,快排时间复杂度会退化成O(N^2),选取left,right,middle中既不是最大也不是最小的值作为keyi
	int middleIndex = MiddleIndex(a, left, right);
	Swap(&a[left], &a[middleIndex]);
	int keyi = left;
	while (left < right)
	{
		// 找比a[keyi]小的数
		while (left < right && a[right] >= a[keyi])
			right--;
		// 找比a[keyi] 大的数
		while (left < right && a[left] <= a[keyi])
			left++;
		// 交换
		Swap(&a[left], &a[right]);
	}
	// 和a[keyi]交换,使a[keyi]移动到正确的位置
	Swap(&a[left], &a[keyi]);
	return left;
}

// 挖坑法
int PartSort2(int* a, int left, int right)
{
	int key = a[left];
	while (left < right)
	{
		// 找到比key小的数
		while (left < right && a[right] >= key)
			right--;
		// 放到左边的坑,右边形成新的坑
		a[left] = a[right];
		while (left < right && a[left] <= key)
			left++;
		// 放到右边的坑,左边形成新的坑
		a[right] = a[left];
	}
	a[left] = key;
	return left;
}

// 前后指针法
int PartSort3(int* a, int left, int right)
{
	int middleIndex = MiddleIndex(a, left, right);
	Swap(&a[left], &a[middleIndex]);
	int cur = left + 1, prev = left, keyi = left;
	for (cur = left + 1; cur <= right; cur++)
	{
		if (a[cur] < a[keyi])
		{
			++prev;
			Swap(&a[cur], &a[prev]);
		}
	}
	Swap(&a[prev], &a[keyi]);
	return prev;
}
// 快速排序
void QuickSort(int* a, int begin, int end)
{
	if (begin >= end)
		return;
	// 单趟排序
	int keyi = PartSort3(a, begin, end);

	// 递归排序左边
	QuickSort(a, begin, keyi - 1);
	// 递归排序右边
	QuickSort(a, keyi + 1, end);
}

// 快排非递归
void QuickNonR(int* a,int begin,int end)
{
	Stack st;
	StackInit(&st);
	// 将初始的begin和end入栈
	StackPush(&st, begin);
	StackPush(&st, end);
	while (!StackEmpty(&st))
	{
		// 得到区间的右端点
		int right = StackTop(&st);
		StackPop(&st);
		// 得到区间的左端点
		int left = StackTop(&st);
		StackPop(&st);
		// 对该段区间进行单趟快排
		int keyi = PartSort3(a, left, right);
		// 将左区间的左右端点入栈
		if (left < keyi - 1)
		{
			StackPush(&st, left);
			StackPush(&st,keyi - 1);
		}
		// 将右区间的左右端点入栈
		if (right > keyi + 1)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, right);
		}
	}
	// 销毁栈
	StackDestroy(&st);
}
// 归并排序
void _MergeSort1(int* a, int left, int right, int* tmp)
{
	if (left >= right)
		return;
	int mid = left + (right - left) / 2;
	// 分解区间
	_MergeSort1(a, left, mid, tmp);
	_MergeSort1(a, mid + 1, right, tmp);
	// 归并
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	// 得到a[begin1]和a[begin2]中较小的数,再移动较小的值
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
			tmp[i++] = a[begin1++];
		else
			tmp[i++] = a[begin2++];
	}
	// 将[begin1,end1]剩余的数放到tmp数组中
	while (begin1 <= end1)
		tmp[i++] = a[begin1++];
	// 将[begin1,end1]剩余的数放到tmp数组中
	while (begin2 <= end2)
		tmp[i++] = a[begin2++];
	// 将tmp数组的值拷贝到a数组中
	memcpy(a, tmp, sizeof(int) * i);
}
void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	_MergeSort1(a, 0, n - 1, tmp);
}

void _MergeSort2(int* a,int* tmp,int begin1,int end1,int begin2,int end2)
{
	int i = begin1;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
			tmp[i++] = a[begin1++];
		else
			tmp[i++] = a[begin2++];
	}
	while (begin1 <= end1)
		tmp[i++] = a[begin1++];
	while (begin2 <= end2)
		tmp[i++] = a[begin2++];
	memcpy(a, tmp, sizeof(int) * i);
}
// 归并排序非递归
void MergeSortNonR(int* a,int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	int gap = 1, i = 0;
	while (gap < n)
	{
		for (i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1, begin2 = i + gap, end2 = i + 2 * gap - 1;
			if (begin2 >= n)
				break;
			if (end2 >= n)
				end2 = n - 1;
			_MergeSort2(a, tmp, begin1, end1, begin2, end2);
		}
		gap *= 2;
	}
}

// 计数排序
void CountSort(int* a, int n)
{
	int i = 0, max = a[0], min = a[0];
	for (i = 0; i < n; i++)
	{
		if (a[i] > max)
			max = a[i];
		if (a[i] < min)
			min = a[i];
	}
	int range = max - min + 1;
	int* count = (int*)malloc(sizeof(int)* range);
	memset(count, 0, sizeof(int) * range);
	for (i = 0; i < n; i++)
	{
		count[a[i] - min]++;
	}
	int j = 0;
	for (i = 0; i < range; i++)
	{
		while (count[i]--)
		{
			a[j++] = i + min;
		}
	}
}
int main()
{
	int a[] = {10,6,7,1,3,9,4,2,5,3,4};
	//QuickSort(a, 0, sizeof(a) / sizeof(a[0]) - 1);
	//QuickNonR(a, 0, sizeof(a) / sizeof(a[0]) - 1);
	//MergeSort(a, sizeof(a) / sizeof(a[0]));
	//BubbleSort(a, sizeof(a) / sizeof(a[0]));
	CountSort(a, sizeof(a) / sizeof(a[0]));
	//MergeSortNonR(a, sizeof(a) / sizeof(a[0]));
	PrintArray(a, sizeof(a) / sizeof(a[0]));

}