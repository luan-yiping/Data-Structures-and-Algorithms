#include"SList.h"
void TestSList1()
{
	SListNode* plist = NULL;
	// β
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);

	SListPrint(plist);

	// βɾ
	SListPopBack(&plist);
	SListPopBack(&plist);
	SListPrint(plist);
	SListPopBack(&plist);
	SListPopBack(&plist);
	SListPopBack(&plist);
	SListPrint(plist);

	// 
	SListDestory(plist);

}
void TestSList2()
{
	SListNode* plist = NULL;

	// ͷ
	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);

	SListPrint(plist);

	// ͷɾ

	SListPopFront(&plist);
	SListPopFront(&plist);
	SListPrint(plist);
	SListPopFront(&plist);
	SListPopFront(&plist);
	SListPopFront(&plist);


	SListPrint(plist);

	SListDestory(plist);

}
int main()
{
	TestSList1();
	TestSList2();
}