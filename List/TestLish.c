#include"List.h"
void TestList01()
{
	ListNode* plist = ListInit();
	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	Listprint(plist);

	ListPopBack(plist);
	ListPopBack(plist);
	ListPopBack(plist);
	ListPopBack(plist);
	// ListPopBack(plist);
	Listprint(plist);

	ListPushFront(plist, 1);
	ListPushFront(plist, 2);
	ListPushFront(plist, 3);
	ListPushFront(plist, 4);
	Listprint(plist);

	ListPopFront(plist);
	ListPopFront(plist);
	ListPopFront(plist);
	ListPopFront(plist);
	// ListPopFront(plist);
	Listprint(plist);
}
void TestList02()
{
	ListNode* plist = ListInit();
	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);

	ListNode* pos = ListFind(plist, 3);
	ListInsert(pos, 30);
	Listprint(plist);
	ListErase(pos);
	Listprint(plist);

}
int main()
{
	// TestList01();
	TestList02();
}