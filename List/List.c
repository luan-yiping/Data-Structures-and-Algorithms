#include"List.h"
// 初始化创建头结点
ListNode* ListInit()
{
	ListNode* phead = BuyListNode(0);
	phead->next = phead;
	phead->prev = phead;
	return phead;
}
// 动态申请一个结点
ListNode* BuyListNode(LTDataType x)
{
	ListNode* NewNode = (ListNode*)malloc(sizeof(ListNode));
	if (NewNode == NULL)
	{
		printf("分配内存失败\n");
		exit(-1);
	}
	NewNode->prev = NewNode->next = NULL;
	NewNode->data = x;
	return NewNode;
}
// 打印
void Listprint(ListNode* plist)
{
	assert(plist);
	ListNode* cur = plist->next;
	while (cur != plist)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}
// 尾插
void ListPushBack(ListNode* plist, LTDataType x)
{
	assert(plist);
	ListNode* NewNode = BuyListNode(x);
	ListNode* tail = plist->prev;
	tail->next = NewNode;
	NewNode->prev = tail;
	plist->prev = NewNode;
	NewNode->next = plist;
}
// 尾删
void ListPopBack(ListNode* plist)
{
	assert(plist);
	assert(plist->next != plist);
	ListNode* tail = plist->prev;
	ListNode* tail_prev = tail->prev;
	tail_prev->next = plist;
	plist->prev = tail_prev;
	free(tail);
	tail = NULL;
}
// 头插
void ListPushFront(ListNode* plist, LTDataType x)
{
	assert(plist);
	ListNode* NewNode = BuyListNode(x);
	ListNode* first = plist->next;
	plist->next = NewNode;
	NewNode->prev = plist;
	NewNode->next = first;
	first->prev = NewNode;
}
// 头删
void ListPopFront(ListNode* plist)
{
	assert(plist);
	assert(plist->next != plist);
	ListNode* first = plist->next;
	ListNode* second = first->next;
	plist->next = second;
	second->prev = plist;
	free(first);
	first = NULL;
}
// 查找
ListNode* ListFind(ListNode* plist, LTDataType x)
{
	assert(plist);
	ListNode* cur = plist->next;
	while (cur != plist)
	{
		if (cur->data == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}
// 在pos位置前插入
void ListInsert(ListNode* pos, LTDataType x)
{
	assert(pos);
	ListNode* NewNode = BuyListNode(x);
	ListNode* PosPrev = pos->prev;
	PosPrev->next = NewNode;
	NewNode->prev = PosPrev;
	NewNode->next = pos;
	pos->prev = NewNode;
}
// 删除pos位置的结点
void ListErase(ListNode* pos)
{
	assert(pos);
	ListNode* PosPrev = pos->prev;
	ListNode* PosNext = pos->next;
	PosPrev->next = PosNext;
	PosNext->prev = PosPrev;
	free(pos);
	pos = NULL;
}
