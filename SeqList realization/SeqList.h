#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#define INITIAL_CAPACITY 4
typedef int SLDataType;
typedef struct SeqList
{
	SLDataType* m_a;
	int m_size; // 有效数据
	int m_capacity; // 容量
}SeqList;
// 顺序表初始化
void SeqListInit(SeqList* psl);
// 顺序表销毁
void SeqListDestory(SeqList* psl);
// 打印
void print(SeqList* psl);
// 尾插
void SeqListPushBack(SeqList* psl, SLDataType x);
// 尾删
void SeqListPopBack(SeqList* psl);
// 头插
void SeqListPushFront(SeqList* psl, SLDataType x);
// 头删
void SeqListPopFront(SeqList* psl);
// 顺序表查找
int SeqListFind(SeqList* psl, SLDataType x);
// 顺序表在pos位置插入x
void SeqListInsert(SeqList* ps, size_t pos, SLDataType x);
// 顺序表删除pos位置的值
void SeqListErase(SeqList* ps, size_t pos);
// 顺序表增容
void SeqListIncreaseCapacity(SeqList* psl);