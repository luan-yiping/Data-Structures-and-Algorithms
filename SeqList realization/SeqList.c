#include"SeqList.h"
// ˳�����ʼ��
void SeqListInit(SeqList* psl)
{
	assert(psl);
	psl->m_a = (SLDataType*)malloc(sizeof(SLDataType) * INITIAL_CAPACITY);
	if (psl->m_a == NULL)
	{
		printf("�����ڴ�ʧ��\n");
		exit(-1);
	}
	psl->m_size = 0;
	psl->m_capacity = INITIAL_CAPACITY;
}
// ˳�������
void SeqListDestory(SeqList* psl)
{
	free(psl->m_a);
	psl->m_a == NULL;
	psl->m_size = psl->m_capacity = 0;
}
// ��ӡ
void print(SeqList* psl)
{
	assert(psl);
	assert(psl->m_size > 0);
	int i = 0;
	for (i = 0; i < psl->m_size; i++)
	{
		printf("%d ", psl->m_a[i]);
	}
	printf("\n");
}
// ˳�������
void SeqListIncreaseCapacity(SeqList* psl)
{
	assert(psl);
	psl->m_capacity *= 2;
	SLDataType* tmp = (SLDataType*)realloc(psl->m_a, sizeof(SLDataType) * (psl->m_capacity));
	if (tmp == NULL)
	{
		printf("����ʧ��\n");
		exit(-1);
	}
	psl->m_a = tmp;
}
// β��
void SeqListPushBack(SeqList* psl, SLDataType x)
{
	assert(psl);
	if (psl->m_size == psl->m_capacity)
	{
		SeqListIncreaseCapacity(psl);
	}
	psl->m_a[psl->m_size++] = x;
}
// βɾ
void SeqListPopBack(SeqList* psl)
{
	assert(psl);
	assert(psl->m_size > 0);
	psl->m_size--;
}
// ͷ��
void SeqListPushFront(SeqList* psl, SLDataType x)
{
	assert(psl);
	if (psl->m_size == psl->m_capacity)
	{
		SeqListIncreaseCapacity(psl);
	}
	int i = psl->m_size - 1;
	for (;i >= 0;i--)
	{
		psl->m_a[i + 1] = psl->m_a[i];
	}
	psl->m_a[0] = x;
	psl->m_size++;
}
// ͷɾ
void SeqListPopFront(SeqList* psl)
{
	assert(psl);
	assert(psl->m_size > 0);
	int i = 1;
	for (i = 1; i <= psl->m_size - 1; i++)
	{
		psl->m_a[i - 1] = psl->m_a[i];
	}
	psl->m_size--;
}
// ˳������ҷ����±�,���Ҳ�������-1
int SeqListFind(SeqList* psl, SLDataType x)
{
	assert(psl);
	assert(psl->m_size > 0);
	int i = 0;
	for (i = 0; i < psl->m_size; i++)
	{
		if (psl->m_a[i] == x)
		{
			return i;
		}
	}
	printf("�޴�����\n");
	return -1;
}
// ˳�����posλ�ò���x
void SeqListInsert(SeqList* psl, size_t pos, SLDataType x)
{
	assert(psl);
	assert(pos >= 0);
	int i = psl->m_size - 1;
	for (; i >= pos; i--)
	{
		psl->m_a[i + 1] = psl->m_a[i];
	}
	psl->m_a[pos] = x;
	psl->m_size++;
}
// ˳���ɾ��posλ�õ�ֵ
void SeqListErase(SeqList* psl , size_t pos)
{
	assert(psl);
	assert(pos >= 0);
	int i = pos + 1;
	for (; i <= psl->m_size - 1; i++)
	{
		psl->m_a[i - 1] = psl->m_a[i];
	}
	psl->m_size--;
}
