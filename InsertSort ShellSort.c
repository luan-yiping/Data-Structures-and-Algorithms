#include<stdio.h>
void InsertSort(int* a, int n)
{
	int i = 0;
	for (i = 0; i < n - 1; i++)
	{
		// 有序区间[0,end]
		int end = i;
		// 将a[end + 1]插入到有序区间里
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (tmp < a[end])
			{
				// 将数据向后移动
				a[end + 1] = a[end];
				--end;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}
void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		// 最终gap为1进行直接插入排序
		gap = (gap / 3) + 1;
		int i = 0;
		// 对gap组数据进行希尔排序
		for (i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					--end;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

// 选择排序
void Swap(int* e1, int* e2)
{
	int tmp = *e1;
	*e1 = *e2;
	*e2 = tmp;
}
void SelectSort(int* a, int n)
{
	int left = 0, right = n - 1;
	int i = left, minIndex = left, maxIndex = left;
	while (left < right)
	{
		for (i = left; i <= right; i++)
		{
			if (a[i] < a[minIndex])
			{
				minIndex = i;
			}
			if (a[i] > a[maxIndex])
			{
				maxIndex = i;
			}
			if (maxIndex == left)
			{
				maxIndex = minIndex;
			}
			Swap(&a[left], &a[minIndex]);
			Swap(&a[right], &a[maxIndex]);
			left++;
			right--;
		}
	}
}

// 堆排序
void AdjustDown(int* a, int parent, int n)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] > a[child])
			++child;
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
	}
}
void HeapSort(int* a, int n)
{
	int i = (n - 1 - 1) / 2;
	// 建堆
	for (i = (n - 2) / 2; i >= 0; i--)
	{
		AdjustDown(a, i, n);
	}
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, 0, end);
		end--;
	}
}



