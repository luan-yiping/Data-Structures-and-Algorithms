#pragma once
#include<iostream>
#include<string>
#include <fstream>
#include<iomanip> // 控制数据输出格式
#include<vector>
#include <algorithm>
using namespace std;
#define MAXSIZE 100
class Book
{
private:
	string login_num;	//登录号
	string book_name;
	string author;
	string type_num;	//分类号
	string publish;		//出版单位
	string data;		//出版时间
	float price;
public:
	Book()
	{
		login_num = "";	//登录号
		book_name = "";
		author = "";
		type_num = "";	//分类号
		publish = "";		//出版单位
		data = "";		//出版时间
		price = 0.0;
	}
    
	Book(string login_num,
		string book_name,
		string author,
		string type_num,
		string publish,
		string data,
		float price)
	{
		this->login_num = login_num;	//登录号
		this->book_name = book_name;
		this->author = author;
		this->type_num = type_num;	//分类号
		this->publish = publish;		//出版单位
		this->data = data;		//出版时间
		this->price = price;
	}
	void set_login_num(string login_num)
	{
		this->login_num = login_num;
	}

	void set_book_name(string book_name)
	{
		this->book_name = book_name;
	}

	void set_author(string author)
	{
		this->author = author;
	}

	void set_type_num(string type_num)
	{
		this->type_num = type_num;
	}

	void set_publish(string publish)
	{
		this->publish = publish;
	}

	void set_data(string data)
	{
		this->data = data;
	}

	void set_price(float price)
	{
		this->price = price;
	}

	string get_login_num()
	{
		return this->login_num;
	}

	string get_book_name()
	{
		return this->book_name;
	}

	string get_author()
	{
		return this->author;
	}

	string get_type_num()
	{
		return this->type_num;
	}

	string get_publish()
	{
		return this->publish;
	}

	string get_data()
	{
		return this->data;
	}

	float get_price()
	{
		return this->price;
	}
};
class MClass
{
private:
	vector<Book> m_stu;
public:
	MClass() { readInfo(); }
	void readInfo();	//读取信息
	bool saveinfo();	//保存信息
	void addStu();		//添加信息
	void display(int way = 0);		//显示所有信息
	void searchStu();	//查询记录
	void delStu();		//删除学生
	void modifyStu();	//修改信息
	void sortStu();		//统计排序
	void run();
	void menu();
};