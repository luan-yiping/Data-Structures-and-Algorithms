#include"Ls.h"

void MClass::menu()
{
	cout << "\t\t\t**********************图书信息管理系统****************" << endl;
	cout << "\t\t\t***                  1.添加书籍记录                ***" << endl;
	cout << "\t\t\t***                  2.显示书籍记录                ***" << endl;
	cout << "\t\t\t***                  3.删除书籍记录                ***" << endl;
	cout << "\t\t\t***                  4.修改书籍记录                ***" << endl;
	cout << "\t\t\t***                  5.查询书籍记录                ***" << endl;
	cout << "\t\t\t***                  6.书本排序                    ***" << endl;
	cout << "\t\t\t***                  7.退出                        ***" << endl;
	cout << "\t\t\t******************************************************" << endl;
}

void MClass::run()
{
	int choose;
	while (true)
	{
		system("cls");
		menu();
		cout << endl << "请输入你的选择:";
		cin >> choose;
		system("cls");
		switch (choose)
		{
		case 1:
			addStu();
			break;
		case 2:
			display();
			break;
		case 3:
			delStu();
			break;
		case 4:
			modifyStu();
			break;
		case 5:
			searchStu();
			break;
		case 6:
			sortStu();
			break;
		case 7:
			return;
		}
		system("pause");

	}

}


bool booksx(Book& stItem1, Book& stItem2)//升序
{
	return stItem1.get_book_name() < stItem2.get_book_name();
}

bool pricesx(Book& stItem1, Book& stItem2)		//升序
{
	return stItem1.get_price() < stItem2.get_price();
}

bool bookjx(Book& stItem1, Book& stItem2)//降序
{
	return stItem1.get_book_name() > stItem2.get_book_name();
}

bool pricejx(Book& stItem1, Book& stItem2)		//降序
{
	return stItem1.get_price() > stItem2.get_price();
}

void MClass::sortStu()
{
	cout << endl << "\t\t\t***                  书本信息排序                    ***" << endl;
	cout << "\t┌─────────┐\n";
	cout << "\t│1--按价格升序排序 │\n";
	cout << "\t│2--按书名升序排序 │\n";
	cout << "\t│3--按价格降序排序 │\n";
	cout << "\t│4--按书名降序排序 │\n";
	cout << "\t└─────────┘\n";
	cout << "请输入你要统计的方式:";
	int choose;
	cin >> choose;
	if (choose == 1)
	{
		sort(m_stu.begin(), m_stu.end(), pricesx);
		display();
	}
	else if (choose == 2)
	{
		sort(m_stu.begin(), m_stu.end(), booksx);
		display(1);
	}
	else if (choose == 3)
	{
		sort(m_stu.begin(), m_stu.end(), pricejx);
		display();
	}
	else if (choose == 4)
	{
		sort(m_stu.begin(), m_stu.end(), bookjx);
		display(1);
	}
}

void MClass::modifyStu()
{
	cout << endl << "\t\t\t***                  书籍信息修改                    ***" << endl;
	cout << "\t┌──────┐\n";
	cout << "\t│1----按书名 │\n";
	cout << "\t│2----按登录号 │\n";
	cout << "\t└──────┘\n";
	cout << "请输入你要修改的方式:";
	int choose;
	cin >> choose;
	string info;
	if (choose == 1)
		cout << endl << "请输入书名:";
	else
		cout << endl << "请输入登录号:";
	cin >> info;

	bool isfind = false;
	for (int i = 0; i < m_stu.size(); i++)
	{
		if (choose == 1 && m_stu[i].get_book_name() == info)
		{
			if (isfind == false)
			{
				cout << endl << setw(10) << "登录号" << setw(20) << "书名" << setw(10) << "作者名" << setw(10) << "分类号" << setw(20) << "出版单位" << setw(16) << "出版时间" << setw(10) << "价格" << endl << endl;
				isfind = true;
			}
			cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;
			int sure = 0;
			cout << endl << "是否修改?(0否|1是):";
			cin >> sure;
			if (sure)
			{
				string tmp;
				float price;
				cout << endl << "请输入登录号:";
				cin >> tmp;
				m_stu[i].set_login_num(tmp);
				cout << endl << "请输入书名:";
				cin >> tmp;
				m_stu[i].set_book_name(tmp);
				cout << endl << "请输入作者名:";
				cin >> tmp;
				m_stu[i].set_author(tmp);
				cout << endl << "请输入分类号:";
				cin >> tmp;
				m_stu[i].set_type_num(tmp);
				cout << endl << "请输入出版单位:";
				cin >> tmp;
				m_stu[i].set_publish(tmp);
				cout << endl << "请输入出版时间:";
				cin >> tmp;
				m_stu[i].set_data(tmp);
				cout << endl << "请输入价格:";
				cin >> price;
				m_stu[i].set_price(price);
				if (saveinfo())
					cout << endl << "修改成功!" << endl << endl;
				else
					cout << endl << "修改失败!" << endl << endl;
			}
		}
		else if (choose == 2 && m_stu[i].get_login_num() == info)
		{
			if (isfind == false)
			{
				cout << endl << setw(10) << "登录号" << setw(20) << "书名" << setw(10) << "作者名" << setw(10) << "分类号" << setw(20) << "出版单位" << setw(16) << "出版时间" << setw(10) << "价格" << endl << endl;
				isfind = true;
			}
			cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;
			int sure = 0;
			cout << endl << "是否修改?(0否|1是):";
			cin >> sure;
			if (sure)
			{
				string tmp;
				float price;
				cout << endl << "请输入登录号:";
				cin >> tmp;
				m_stu[i].set_login_num(tmp);
				cout << endl << "请输入书名:";
				cin >> tmp;
				m_stu[i].set_book_name(tmp);
				cout << endl << "请输入作者名:";
				cin >> tmp;
				m_stu[i].set_author(tmp);
				cout << endl << "请输入分类号:";
				cin >> tmp;
				m_stu[i].set_type_num(tmp);
				cout << endl << "请输入出版单位:";
				cin >> tmp;
				m_stu[i].set_publish(tmp);
				cout << endl << "请输入出版时间:";
				cin >> tmp;
				m_stu[i].set_data(tmp);
				cout << endl << "请输入价格:";
				cin >> price;
				m_stu[i].set_price(price);
				if (saveinfo())
					cout << endl << "修改成功!" << endl << endl;
				else
					cout << endl << "修改失败!" << endl << endl;
			}
		}

	}
	if (!isfind)
		cout << endl << "该书本不存在!" << endl << endl;
}


void MClass::delStu()
{
	cout << endl << "\t\t\t***                  书本信息删除                    ***" << endl;
	cout << "\t┌──────┐\n";
	cout << "\t│1----按书名 │\n";
	cout << "\t│2--按登录号 │\n";
	cout << "\t└──────┘\n";
	cout << "请输入你要删除的方式:";
	int choose;
	cin >> choose;
	string info;
	if (choose == 1)
		cout << endl << "请输入书名:";
	else
		cout << endl << "请输入登录号:";
	cin >> info;

	bool isfind = false;
	for (int i = 0; i < m_stu.size(); i++)
	{
		if (choose == 1 && m_stu[i].get_book_name() == info)
		{
			if (isfind == false)
			{
				cout << endl << setw(10) << "登录号" << setw(20) << "书名" << setw(10) << "作者名" << setw(10) << "分类号" << setw(20) << "出版单位" << setw(16) << "出版时间" << setw(10) << "价格" << endl << endl;
				isfind = true;
			}
			cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;
			int sure = 0;
			cout << endl << "是否删除?(0否|1是):";
			cin >> sure;
			if (sure)
			{
				for (vector<Book>::iterator it = m_stu.begin(); it != m_stu.end();)
				{
					if ((*it).get_book_name() == info)
					{
						it = m_stu.erase(it);    //删除元素，返回值指向已删除元素的下一个位置
						break;
					}
					else
						++it;    //指向下一个位置
				}
				if (saveinfo())
					cout << endl << "删除成功!" << endl << endl;
				else
					cout << endl << "删除失败!" << endl << endl;
			}
		}
		else if (choose == 2 && m_stu[i].get_login_num() == info)
		{
			if (isfind == false)
			{
				cout << endl << setw(10) << "登录号" << setw(20) << "书名" << setw(10) << "作者名" << setw(10) << "分类号" << setw(20) << "出版单位" << setw(16) << "出版时间" << setw(10) << "价格" << endl << endl;
				isfind = true;
			}
			cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;
			int sure = 0;
			cout << endl << "是否删除?(0否|1是):";
			cin >> sure;
			if (sure)
			{
				for (vector<Book>::iterator it = m_stu.begin(); it != m_stu.end();)
				{
					if ((*it).get_login_num() == info)
					{
						it = m_stu.erase(it);    //删除元素，返回值指向已删除元素的下一个位置
						break;
					}
					else
						++it;    //指向下一个位置
				}
				if (saveinfo())
					cout << endl << "删除成功!" << endl << endl;
				else
					cout << endl << "删除失败!" << endl << endl;
			}
		}

	}
	if (!isfind)
		cout << endl << "该书本不存在!" << endl << endl;
}

void MClass::searchStu()
{
	cout << endl << "\t\t\t***                  书本信息查询                    ***" << endl;
	cout << "\t┌──────┐\n";
	cout << "\t│1----按书名 │\n";
	cout << "\t│2----按作者 │\n";
	cout << "\t└──────┘\n";
	cout << "请输入你要查询的方式:";
	int choose;
	cin >> choose;
	string info;
	if (choose == 1)
		cout << endl << "请输入书名:";
	else
		cout << endl << "请输入作者:";
	cin >> info;

	bool isfind = false;
	for (int i = 0; i < m_stu.size(); i++)
	{
		if (choose == 1 && m_stu[i].get_book_name() == info)
		{
			if (isfind == false)
			{
				cout << endl << setw(10) << "登录号" << setw(20) << "书名" << setw(10) << "作者名" << setw(10) << "分类号" << setw(20) << "出版单位" << setw(16) << "出版时间" << setw(10) << "价格" << endl << endl;
				isfind = true;
			}
			cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;
		}
		else if (choose == 2 && m_stu[i].get_author() == info)
		{
			if (isfind == false)
			{
				cout << endl << setw(10) << "登录号" << setw(20) << "书名" << setw(10) << "作者名" << setw(10) << "分类号" << setw(20) << "出版单位" << setw(16) << "出版时间" << setw(10) << "价格" << endl << endl;
				isfind = true;
			}
			cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;
		}

	}
	if (!isfind)
		cout << endl << "该书本不存在!" << endl << endl;
}

void MClass::display(int way)
{
	if (m_stu.size() <= 0)
	{
		cout << endl << "当前没有信息!" << endl << endl;
		return;
	}
	cout << endl << "\t\t\t***                  书本信息展示                    ***" << endl;
	cout << endl << setw(10) << "登录号" << setw(20) << "书名" << setw(10) << "作者名" << setw(10) << "分类号" << setw(20) << "出版单位" << setw(16) << "出版时间" << setw(10) << "价格" << endl << endl;
	for (int i = 0; i < m_stu.size(); i++)
	{
		//if (way == 0)
		cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;
		/*else if (way == 1 && (m_stu[i].Language < 60 || m_stu[i].Math < 60 || m_stu[i].English < 60))
			cout << setw(10) << m_stu[i].get_login_num() << setw(20) << m_stu[i].get_book_name() << setw(10) << m_stu[i].get_author() << setw(10) << m_stu[i].get_type_num() << setw(20) << m_stu[i].get_publish() << setw(16) << m_stu[i].get_data() << setw(10) << m_stu[i].get_price() << endl << endl;*/
	}
}
void MClass::addStu()
{
	cout << endl << "\t\t\t***                  书本信息添加                    ***" << endl;

	Book m;

	string tmp;
	float price;
	cout << endl << "请输入登录号:";
	cin >> tmp;
	m.set_login_num(tmp);
	cout << endl << "请输入书名:";
	cin >> tmp;
	m.set_book_name(tmp);
	cout << endl << "请输入作者名:";
	cin >> tmp;
	m.set_author(tmp);
	cout << endl << "请输入分类号:";
	cin >> tmp;
	m.set_type_num(tmp);
	cout << endl << "请输入出版单位:";
	cin >> tmp;
	m.set_publish(tmp);
	cout << endl << "请输入出版时间:";
	cin >> tmp;
	m.set_data(tmp);
	cout << endl << "请输入价格:";
	cin >> price;
	m.set_price(price);

	m_stu.push_back(m);

	if (saveinfo())
		cout << endl << "添加成功!" << endl << endl;
	else
		cout << endl << "添加失败!" << endl << endl;
}

bool MClass::saveinfo()
{
	ofstream f1;
	int i;
	f1.open("BookInfo.txt");
	if (!f1)
	{
		return false;
	}
	for (i = 0; i < m_stu.size(); i++)
	{
		f1 << m_stu[i].get_login_num() << " " << m_stu[i].get_book_name() << " " << m_stu[i].get_author() << " " << m_stu[i].get_type_num() << " " << m_stu[i].get_publish() << " " << m_stu[i].get_data() << " " << m_stu[i].get_price() << endl;
	}
	f1.close();
	return true;
}

void MClass::readInfo()
{
	ifstream f1;
	string login_num;	//登录号
	string book_name;
	string author;
	string type_num;	//分类号
	string publish;		//出版单位
	string data;		//出版时间
	float price;
	Book b;

	int i;
	f1.open("BookInfo.txt");
	if (!f1)
	{
		return;
	}
	while (!f1.eof())
	{
		f1 >> login_num >> book_name >> author >> type_num >> publish >> data >> price;
		if (f1.eof())
			break;
		b.set_login_num(login_num);
		b.set_book_name(book_name);
		b.set_author(author);
		b.set_type_num(type_num);
		b.set_publish(publish);
		b.set_data(data);
		b.set_price(price);
		m_stu.push_back(b);
	}
	f1.close();
}
int main()
{
	MClass test;
	test.run();
	return 0;
}