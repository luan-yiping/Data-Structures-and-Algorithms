typedef struct TreeNode TreeNode;
// �ݹ�
void InOrder(TreeNode* root, int* ret, int* size)
{
    if (root == NULL)
        return;
    InOrder(root->left, ret, size);
    ret[(*size)++] = root->val;
    InOrder(root->right, ret, size);
}
int* inorderTraversal(struct TreeNode* root, int* returnSize)
{
    int* ret = (int*)malloc(sizeof(int) * 100);
    int size = 0;
    InOrder(root, ret, &size);
    *returnSize = size;
    return ret;
}

// ����
int* inorderTraversal(struct TreeNode* root, int* returnSize)
{
    TreeNode** Stack = (TreeNode**)malloc(sizeof(TreeNode*) * 100);
    int* ret = (int*)malloc(sizeof(int) * 100);
    int top = 0;
    *returnSize = 0;
    TreeNode* cur = root;
    while (top > 0 || cur != NULL)
    {
        while (cur != NULL)
        {
            Stack[top++] = cur;
            cur = cur->left;
        }
        cur = Stack[--top];
        ret[(*returnSize)++] = cur->val;
        cur = cur->right;
    }
    return ret;
}