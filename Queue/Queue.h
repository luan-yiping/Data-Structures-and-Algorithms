#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
typedef int QDataType;
typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QueueNode;
typedef struct Queue
{
	QueueNode* head;
	QueueNode* tail;
}Queue;
// 队列初始化
void QueueInit(Queue* pq);
// 入队
void QueuePush(Queue* pq, QDataType x);
// 出队
void QueuePop(Queue* pq);
// 判空
bool QueueEmpty(Queue* pq);
// 获取队尾元素
QDataType QueueBack(Queue* pq);
// 获取队头元素
QDataType QueueFront(Queue* pq);
// 销毁队列
void QueueDestroy(Queue* pq);
// 队列元素个数
int QueueSize(Queue* pq);