#include"Stack.h"
void TestStack01()
{
	Stack st;
	StackInit(&st);
	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	StackPush(&st, 4);
	print(&st);
	
	StackPop(&st);
	print(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);

	
	StackDestroy(&st);

}
int main()
{
	TestStack01();
}
