#include"Stack.h"

// 打印
void print(Stack* pst)
{
	assert(pst);
	int i = 0;
	for (i = 0; i < pst->top; i++)
	{
		printf("%d ", pst->a[i]);
	}
	printf("\n");
}
// 判断栈是否为空，为空返回true,非空返回false
bool StackEmpty(Stack* pst)
{
	assert(pst);
	return pst->top == 0;
}
// 栈的初始化
void StackInit(Stack* pst)
{
	assert(pst);
	pst->a = (STDataType*)malloc(sizeof(STDataType) * INIT_CAPACITY);
	if (pst->a == NULL)
	{
		printf("malloc failed\n");
		exit(-1);
	}
	pst->top = 0;
	pst->capacity = INIT_CAPACITY;
}
// 入栈
void StackPush(Stack* pst, STDataType x)
{
	assert(pst);
	// 动态增容
	if (pst->top == pst->capacity)
	{
		pst->capacity *= 2;
		STDataType* tmp = (STDataType*)realloc(pst->a, sizeof(STDataType) * pst->capacity);
		if (tmp == NULL)
		{
			printf("realloc failed\n");
			exit(-1);
		}
		pst->a = tmp;
	}
	pst->a[pst->top] = x;
	pst->top++;
}
// 出栈
void StackPop(Stack* pst)
{
	assert(pst);
	assert(!StackEmpty(pst));
	pst->top--;
}
// 获取栈顶元素
STDataType StackTop(Stack* pst)
{
	assert(pst);
	assert(!StackEmpty(pst));
	return pst->a[pst->top - 1];
}
// 获取栈的元素个数
int StackSize(Stack* pst)
{
	assert(pst);
	return pst->top;
}
// 销毁栈
void StackDestroy(Stack* pst)
{
	assert(pst);
	pst->top = pst->capacity = 0;
	free(pst->a);
	pst->a = NULL;
}