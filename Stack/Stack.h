#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdbool.h>
#include<stdlib.h>
#define INIT_CAPACITY 4
typedef int STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;  // 栈顶下标
	int capacity; // 栈容量
}Stack;
// 栈的初始化
void StackInit(Stack* pst);
// 入栈
void StackPush(Stack* pst, STDataType x);
// 出栈
void StackPop(Stack* pst);
// 获取栈顶元素
STDataType StackTop(Stack* pst);
// 获取栈的元素个数
int StackSize(Stack* pst);
// 判断栈是否为空，为空返回true,非空返回false
bool StackEmpty(Stack* pst);
// 打印
void print(Stack* pst);
// 销毁栈
void StackDestroy(Stack* pst);