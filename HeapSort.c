
#include<stdio.h>
void Swap(int* child, int* parent)
{
	int tmp = *child;
	*child = *parent;
	*parent = tmp;
}
// 左右子树必须是堆
void AdjustDown(int* a,int n,int parent)
{
	int child = 2 * parent + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] > a[child])
			++child;
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			break;
		}
	}
}
// 排升序,建大堆
// 排降序,建小堆
void HeapSort(int* a, int n)
{
	int end = n - 1;
	while(end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}
int main()
{
	int a[] = {15,18,28,34,65,19,49,25,37,27};
	int len = sizeof(a) / sizeof(a[0]);
	// 建堆
	int i = (len - 1 - 1) / 2;
	for (i = (len - 2) / 2; i >= 0; i--)
	{
		AdjustDown(a, len, i);
	}
	HeapSort(a, len);
}

