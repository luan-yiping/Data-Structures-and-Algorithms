#include<iostream>
#include<set>
#include<map>
using namespace std;
void print(const set<int>& s)
{
	/*
	for (const auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;*/

	for (set<int>::iterator it = s.begin(); it != s.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}
void test1()
{
	set<int> first;
	int arr[] = { 1,2,3 };
	set<int> second(arr, arr + 3);
	set<int> third(second);
	set<int> fourth(second.begin(), second.end());
}
// insert
void test2()
{
	set<int> s;
	set<int>::iterator it;
	pair<set<int>::iterator, bool> ret;

	for (int i = 1; i <= 5; i++) s.insert(i);

	ret = s.insert(3);
	if (ret.second == false) it = ret.first;

	s.insert(it,2);
	s.insert(it,0);
	s.insert(it,10);

	int a[] = {-1,-2,-5};
	s.insert(a,a + 3);

	for (it = s.begin(); it != s.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}
void test3()
{
	set<int> s;
	set<int>::iterator it;
	for (int i = 1; i <= 5; i++) s.insert(i);

	it = s.begin();
	++it;
	s.erase(it);

	size_t ret = s.erase(3);
	cout << ret << endl;

	it = s.find(4);
	s.erase(it,s.end());

	for (it = s.begin(); it != s.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}
void test4()
{
	int a[] = {1,2,3,4,5,6};
	set<int> s1(a,a + 3);
	set<int> s2(a + 3,a + 6);

	s1.swap(s2);
}
void test5()
{
	set<int> s;
	s.insert(100);
	s.insert(200);
	print(s);
	s.clear();
	s.insert(400);
	s.insert(500);
	print(s);
}
void test6()
{
	set<int> s;
	for (int i = 0; i <= 5; i++) s.insert(i);

	set<int>::iterator it;
	for (it = s.begin(); it != s.end(); it++)
	{
		// *it += 1; 注意不能修改
		cout << *it << " ";
	}
	cout << endl;

	set<int>::reverse_iterator rit;
	for (rit = s.rbegin(); rit != s.rend(); rit++)
	{
		cout << *rit << " ";
	}
	cout << endl;
}
void test7()
{
	multiset<int> m;

	for (int i = 0; i <= 5; i++) m.insert(i);
	// multiset允许键值冗余
	m.insert(3);
	m.insert(4);
	m.insert(5);

	multiset<int>::iterator it;

	for (it = m.begin(); it != m.end(); it++)
	{
		// *it += 1; 注意不能修改
		cout << *it << " ";
	}
	cout << endl;

	// find查找的val有多个的时候，找到的是中序的第一个
	multiset<int>::iterator pos = m.find(3);
	while (*pos == 3)
	{
		cout << *pos << endl;
		++pos;
	}

	cout << m.count(3) << endl;

	// erase会删除所有的 3
	m.erase(3);
	for (it = m.begin(); it != m.end(); it++)
	{
		// *it += 1; 注意不能修改
		cout << *it << " ";
	}
	cout << endl;
}
void test8()
{
	map<int, double> m;
	// 调用pair的构造函数，构造一个匿名对象插入
	m.insert(pair<int,double>(1,1.1));
	m.insert(pair<int, double>(2, 2.2));
	m.insert(pair<int, double>(3,3.3));
	// 调用函数模板，构造对象更方便
	m.insert(make_pair(4,4.4));

	map<int, double>::iterator it;
	for (it = m.begin(); it != m.end(); it++)
	{
		cout << it->first << " " << it->second << endl;
	}
}
void test9()
{
	// 构造一个空map
	map<char, int> m;
	m['a'] = 10;
	m['b'] = 20;
	m['c'] = 30;
	// 迭代器区间初始化
	map<char, int> m2(m.begin(),m.end());
	// 拷贝构造
	map<char, int> m3(m2);
}
int main()
{
	//test2();
	//test3();
	//test4();
	//test5();
	//test6();
	//test7();
	test8();
}